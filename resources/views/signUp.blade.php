<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SanberBook | Sign Up</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="post">
        @csrf
        <label>First name:</label> <br><br>
        <input type="text" name="fn"> <br><br>
        <label>Last name:</label> <br><br>
        <input type="text" name="ln"> <br><br>
        <label for="gen">Gender:</label> <br><br>
        <input type="radio" name="gen">Male <br>
        <input type="radio" name="gen">Female <br>
        <input type="radio" name="gen">Other <br><br>
        <label>Nationality:</label><br><br>
        <select name="Nationality">
            <option>Indonesia</option>
            <option>USA</option>
            <option>Japan</option>
            <option>France</option>
            <option>Other</option>
        </select><br><br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox">Bahasa Indonesia<br>
        <input type="checkbox">English<br>
        <input type="checkbox">Other<br><br>
        <label>Bio:</label><br><br>
        <textarea name="Bio" cols="30" rows="10"></textarea><br>
        <input type="submit" value="SIGN UP">


    </form>
</body>
</html>
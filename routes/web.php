<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route bekas tugas 3.2
Route::get('/getstarted', 'HomeController@home'
);

Route::get('/register', 'AuthController@registration'
);

Route::post('/welcome', 'AuthController@includeName'
);

// Route::get('/', function () {
//     return view('adminlte.master');
// });

Route::get('/', 'HomeController@blank'
);

Route::get('/table', 'AuthController@table'
);

Route::get('/data-tables', 'AuthController@dataTable'
);


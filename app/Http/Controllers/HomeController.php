<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home(){
        return view('first');
    }

    public function blank(){
        return view('adminlte.parts.blank');
    }
}
